package it.uniba.gol;

import static org.junit.Assert.*;

import org.junit.Test;

public class GridTest {

	@Test
	public void gridShouldBeAlive() throws Exception {
		Cell [][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0 ,0 ,false);
		cells[0][1] = new Cell(0 ,1 ,false);
		cells[0][2] = new Cell(0 ,2 ,true);

		cells[1][0] = new Cell(1 ,0 ,false);
		cells[1][1] = new Cell(1 ,1 ,true);
		cells[1][2] = new Cell(1 ,2 ,false);
		
		cells[2][0] = new Cell(2 ,0 ,true);
		cells[2][1] = new Cell(2 ,1 ,true);
		cells[2][2] = new Cell(2 ,2 ,false);
		
		Grid grid = new Grid(cells, 3, 3);
	}
	
	@Test(expected = CustomLifeException.class)
	public void gridShouldRaiseExceptionOnHeight() throws Exception {
		Cell [][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0 ,0 ,false);
		cells[0][1] = new Cell(0 ,1 ,false);
		cells[0][2] = new Cell(0 ,2 ,true);

		cells[1][0] = new Cell(1 ,0 ,false);
		cells[1][1] = new Cell(1 ,1 ,true);
		cells[1][2] = new Cell(1 ,2 ,false);
		
		cells[2][0] = new Cell(2 ,0 ,true);
		cells[2][1] = new Cell(2 ,1 ,true);
		cells[2][2] = new Cell(2 ,2 ,false);
		
		Grid grid = new Grid(cells, 3, 0);
	}
	
	@Test(expected = CustomLifeException.class)
	public void gridShouldRaiseExceptionOnWidth() throws Exception {
		Cell [][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0 ,0 ,false);
		cells[0][1] = new Cell(0 ,1 ,false);
		cells[0][2] = new Cell(0 ,2 ,true);

		cells[1][0] = new Cell(1 ,0 ,false);
		cells[1][1] = new Cell(1 ,1 ,true);
		cells[1][2] = new Cell(1 ,2 ,false);
		
		cells[2][0] = new Cell(2 ,0 ,true);
		cells[2][1] = new Cell(2 ,1 ,true);
		cells[2][2] = new Cell(2 ,2 ,false);
		
		Grid grid = new Grid(cells, 0, 3);
	}
	
	@Test(expected = CustomLifeException.class)
	public void gridShouldRaiseExceptionOnXWidth() throws Exception {
		Cell [][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0 ,0 ,false);
		cells[0][1] = new Cell(0 ,1 ,false);
		cells[0][2] = new Cell(0 ,2 ,true);

		cells[1][0] = new Cell(1 ,0 ,false);
		cells[1][1] = new Cell(1 ,1 ,true);
		cells[1][2] = new Cell(1 ,2 ,false);
		
		cells[2][0] = new Cell(2 ,0 ,true);
		cells[2][1] = new Cell(2 ,1 ,true);
		cells[2][2] = new Cell(2 ,2 ,false);
		
		Grid grid = new Grid(cells, 1, 3);
	}
	
	@Test(expected = CustomLifeException.class)
	public void gridShouldRaiseExceptionOnYHeight() throws Exception {
		Cell [][] cells = new Cell[3][3];
		
		cells[0][0] = new Cell(0 ,0 ,false);
		cells[0][1] = new Cell(0 ,1 ,false);
		cells[0][2] = new Cell(0 ,2 ,true);

		cells[1][0] = new Cell(1 ,0 ,false);
		cells[1][1] = new Cell(1 ,1 ,true);
		cells[1][2] = new Cell(1 ,2 ,false);
		
		cells[2][0] = new Cell(2 ,0 ,true);
		cells[2][1] = new Cell(2 ,1 ,true);
		cells[2][2] = new Cell(2 ,2 ,false);
		
		Grid grid = new Grid(cells, 3, 1);
	}
	
}
